package id.ac.binainsani.utils

import android.app.Dialog
import android.content.Context
import android.graphics.BlendMode
import android.graphics.BlendModeColorFilter
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.os.Build
import android.view.LayoutInflater
import androidx.annotation.NonNull
import androidx.core.content.res.ResourcesCompat
import id.ac.binainsani.R

class CaProgress {

    var dialog: Dialog? = null

    fun show(context: Context): Dialog? {

        val inflator = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        val mView = inflator.inflate(R.layout.dialog_progress, null)

        dialog = Dialog(context, R.style.CaProgressBarTheme)

        dialog?.apply {
            if (isShowing) dismiss()
            setContentView(mView)
            show()
        }

        return dialog

    }

    fun dismiss() {
        dialog?.apply {
            if (isShowing) dismiss()
        }
    }

}