package id.ac.binainsani

import android.app.Application
import android.content.Context

@Suppress("Unused")
class App : Application() {

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
//        MultiDex.install(this)
    }
}