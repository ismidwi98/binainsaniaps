package id.ac.binainsani.ui.intro

import android.os.Bundle
import android.view.MenuItem
import androidx.annotation.LayoutRes
import id.ac.binainsani.BaseActivity
import id.ac.binainsani.R
import id.ac.binainsani.databinding.ActivityIntroBinding

class IntroActivity: BaseActivity<ActivityIntroBinding>() {

    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_intro

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupToolbar()
        initializePlayer()
    }

    private fun initializePlayer() {
        binding.youtubePlayerView.enterFullScreen()
        lifecycle.addObserver(binding.youtubePlayerView)
    }

    private fun setupToolbar(){
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Perkenalan"
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}