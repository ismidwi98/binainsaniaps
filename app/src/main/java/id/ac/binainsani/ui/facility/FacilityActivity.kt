package id.ac.binainsani.ui.facility

import android.os.Bundle
import android.view.MenuItem
import androidx.annotation.LayoutRes
import id.ac.binainsani.BaseActivity
import id.ac.binainsani.R
import id.ac.binainsani.databinding.ActivityFacilityBinding
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.startActivity

class FacilityActivity: BaseActivity<ActivityFacilityBinding>() {

    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_facility

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupToolbar()

        binding.btnClassroom.onClick {
            startActivity<PanoramaActivity>("mode" to "classroom.png")
        }

        binding.btnLibrary.onClick {
            startActivity<PanoramaActivity>("mode" to "library.jpg")
        }

        binding.btnComputer.onClick {
            startActivity<PanoramaActivity>("mode" to "labkom.jpg")
        }

        binding.btnSeminar.onClick {
            startActivity<PanoramaActivity>("mode" to "seminar.jpg")
        }

        binding.btnMultimedia.onClick {
            startActivity<PanoramaActivity>("mode" to "multimedia.jpg")
        }
    }

    private fun setupToolbar(){
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Fasilitas"
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId){
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }
}