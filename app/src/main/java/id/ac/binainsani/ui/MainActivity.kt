package id.ac.binainsani.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.MenuItem
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.annotation.LayoutRes
import com.google.android.material.bottomnavigation.BottomNavigationView
import id.ac.binainsani.BaseActivity
import id.ac.binainsani.R
import id.ac.binainsani.databinding.ActivityMainBinding
import id.ac.binainsani.ui.facility.FacilityActivity
import id.ac.binainsani.ui.intro.IntroActivity
import id.ac.binainsani.ui.lokasi.LokasiActivity
import id.ac.binainsani.ui.profile.ProfileActivity
import org.jetbrains.anko.startActivity

class MainActivity: BaseActivity<ActivityMainBinding>(), BottomNavigationView.OnNavigationItemSelectedListener {

    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initWebView()

        binding.bnMain.setOnNavigationItemSelectedListener(this)
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun initWebView() {
        caProgress.show(this@MainActivity)
        binding.webview.apply {
            settings.apply {
                javaScriptEnabled = true
                useWideViewPort = true
                loadWithOverviewMode = true
                domStorageEnabled = true
                mixedContentMode = WebSettings.MIXED_CONTENT_COMPATIBILITY_MODE
            }
            webChromeClient = WebChromeClient()
            webChromeClient = object : WebChromeClient() {
                override fun onProgressChanged(view: WebView, newProgress: Int) {
                    super.onProgressChanged(view, newProgress)

                    if (newProgress == 100) caProgress.dismiss()
                }
            }
            webViewClient = WebViewClient()
            loadUrl("https://binainsani.ac.id/")
        }
    }

    override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            R.id.nav_video -> {
                startActivity<IntroActivity>()
                return true
            }
            R.id.nav_lokasi -> {
                startActivity<LokasiActivity>()
                return true
            }
            R.id.nav_fasilitas -> {
                startActivity<FacilityActivity>()
                return true
            }
            R.id.nav_tentang -> {
                binding.webview.loadUrl("https://binainsani.ac.id/profile/sejarah-perguruan-tinggi-bina-insani/")
            }
            R.id.nav_profile -> {
                startActivity<ProfileActivity>()
                return true
            }
        }

        return false
    }
}