package id.ac.binainsani.ui.facility

import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.MenuItem
import androidx.annotation.LayoutRes
import com.google.vr.sdk.widgets.pano.VrPanoramaView
import id.ac.binainsani.BaseActivity
import id.ac.binainsani.R
import id.ac.binainsani.databinding.ActivityPanoramaBinding
import java.io.InputStream

class PanoramaActivity: BaseActivity<ActivityPanoramaBinding>() {

    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_panorama

    private var mode = "classroom"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        intent.getStringExtra("mode")?.let{
            mode = it
        }

        setupToolbar()

        loadPhotoSphere()
    }

    private fun loadPhotoSphere() {
        val options = VrPanoramaView.Options()
        val inputStream: InputStream?

        val assetManager = assets
        try {
            inputStream = assetManager.open(mode)
            options.inputType = VrPanoramaView.Options.TYPE_MONO
            binding.vrPanoramaView.loadImageFromBitmap(BitmapFactory.decodeStream(inputStream), options)
            inputStream.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun onPause() {
        super.onPause()
        binding.vrPanoramaView.pauseRendering()
    }

    override fun onResume() {
        super.onResume()
        binding.vrPanoramaView.resumeRendering()
    }

    override fun onDestroy() {
        binding.vrPanoramaView.shutdown()
        super.onDestroy()
    }

    private fun setupToolbar(){
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "360 Experience"
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}