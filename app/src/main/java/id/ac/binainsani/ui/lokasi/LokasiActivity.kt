package id.ac.binainsani.ui.lokasi

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.provider.Settings
import android.view.MenuItem
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AlertDialog
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import id.ac.binainsani.BaseActivity
import id.ac.binainsani.R
import id.ac.binainsani.databinding.ActivityLocationBinding
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import com.livinglifetechway.quickpermissions_kotlin.util.QuickPermissionsOptions
import com.livinglifetechway.quickpermissions_kotlin.util.QuickPermissionsRequest
import org.jetbrains.anko.design.indefiniteSnackbar

class LokasiActivity:BaseActivity<ActivityLocationBinding>(), OnMapReadyCallback {

    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_location

    private lateinit var map: GoogleMap

    private val locationPermissionOption = QuickPermissionsOptions(
        handleRationale = true,
        rationaleMethod = { req -> rationaleCallback(req) },
        permanentDeniedMethod = { req -> permissionsPermanentlyDenied(req) }
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupToolbar()

        runWithPermissions(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            options = locationPermissionOption
        ){

            val mMapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
            mMapFragment?.getMapAsync(this)
        }
    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        map.uiSettings.isZoomControlsEnabled = true
        map.uiSettings.isZoomGesturesEnabled = true
        map.uiSettings.isMapToolbarEnabled = true
        map.uiSettings.isMyLocationButtonEnabled = true
        map.isMyLocationEnabled = true

//        deviceLocation()

        val kampus = LatLng(-6.2598341171141145, 106.9946211162923)

        map.clear()
        map.addMarker(MarkerOptions().position(kampus).draggable(true))
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(kampus, 18f))
    }

    @SuppressLint("MissingPermission")
    private fun deviceLocation() {
        LocationServices.getFusedLocationProviderClient(this).lastLocation.addOnSuccessListener {
            if (it != null) {
                toLocation(LatLng(it.latitude, it.longitude))
            } else {
                binding.root.indefiniteSnackbar("Location services is disabled")
                    .setAction("Settings") {
                        startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                    }.setActionTextColor(Color.GREEN).show()
            }
        }
    }

    private fun toLocation(latLng: LatLng) {
        val kampus = LatLng(-6.2598341171141145, 106.9946211162923)

        map.clear()
        map.addMarker(MarkerOptions().position(kampus).draggable(true))
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(kampus, 18f))

//        map.clear()
//        val dusunMarker = MarkerOptions().position(kampus)
//            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_binainsanilogo_1))
//        map.addMarker(dusunMarker)

        val markerBounds = LatLngBounds(latLng, kampus)

        val cu = CameraUpdateFactory.newLatLngBounds(markerBounds,120)
        map.animateCamera(cu)
    }

    private fun rationaleCallback(req: QuickPermissionsRequest) {
        AlertDialog.Builder(this)
            .setTitle("Permissions Denied")
            .setMessage("Please allow us to proceed asking for permissions again, or cancel to end the permission flow.")
            .setPositiveButton("Go Ahead") { _, _ -> req.proceed() }
            .setNegativeButton("cancel") { _, _ -> req.cancel() }
            .setCancelable(false)
            .show()
    }

    private fun permissionsPermanentlyDenied(req: QuickPermissionsRequest) {
        AlertDialog.Builder(this)
            .setTitle("Permissions Denied")
            .setMessage("Please open app settings to open app settings for allowing permissions, " +
                    "or cancel to end the permission flow.")
            .setPositiveButton("App Settings") { _, _ -> req.openAppSettings() }
            .setNegativeButton("Cancel") { _, _ -> req.cancel() }
            .setCancelable(false)
            .show()
    }

    private fun setupToolbar(){
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Lokasi Universitas"
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}