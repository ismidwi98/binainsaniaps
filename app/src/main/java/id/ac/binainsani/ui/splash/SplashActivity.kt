package id.ac.binainsani.ui.splash

import android.os.Bundle
import android.os.Handler
import androidx.annotation.LayoutRes
import id.ac.binainsani.BaseActivity
import id.ac.binainsani.R
import id.ac.binainsani.databinding.ActivitySplashBinding
import id.ac.binainsani.ui.MainActivity
import org.jetbrains.anko.startActivity

class SplashActivity: BaseActivity<ActivitySplashBinding>() {

    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_splash

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Handler().postDelayed({
            startActivity<MainActivity>()
            finish()
        }, 2000)
    }
}